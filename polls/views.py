from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils import timezone

from polls.models import Choice, Poll, Vote

def loginView(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    if username is not None and password is not None:
        user = authenticate(username = username, password = password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('polls:index')
        else:
            # Login failed
            return HttpResponse('Username/password combo is incorrect.')
    else:
        # Render the login form
        return render(request, 'polls/login.html', locals())

def logoutView(request):
    logout(request)
    return redirect('polls:index')

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_poll_list'

    def get_queryset(self):
        """
        Return the last five published polls.
        """
        now = timezone.now()
        polls = Poll.objects.filter(start_time__lte = now, end_time__gte = now, 
                                    pub_date__lte = now) \
                            .order_by('-pub_date')[:5]
        return polls

class DetailView(generic.DetailView):
    model = Poll
    template_name = 'polls/detail.html'

    def get_queryset(self):
        """
        Filter out the polls that are off
        """
        now = timezone.now()
        return Poll.objects.filter(start_time__lte = now, end_time__gte = now)

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        context['has_voted'] = Vote.objects.filter(user = self.request.user, 
                                                   poll = self.get_object()) \
                                           .exists()
        return context

class ResultsView(generic.DetailView):
    model = Poll
    template_name = 'polls/results.html'

    def get_queryset(self):
        """
        Filter out the polls that are off
        """
        now = timezone.now()
        return Poll.objects.filter(start_time__lte = now, end_time__gte = now)

@login_required(login_url='polls:login')
def vote(request, poll_id):
    p = get_object_or_404(Poll, pk=poll_id)
    now = timezone.now()
    if not (p.start_time <= now and p.end_time >= now):
        return redirect('polls:index')
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the poll voting form.
        return render(request, 'polls/detail.html', {
            'poll': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        vote = Vote(user = request.user, poll = p, choice = selected_choice)
        vote.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))